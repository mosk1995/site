// JavaScript Document

function getJSONFile(nameFileJSON){
   var value= $.ajax({ 
      url: nameFileJSON, 
      async: false
   }).responseText;
   return value;
}	
//Откуда то нагло скопированная функция для привязки действия к обьекту с поправкой на работу в IE
function addEvent(obj, evType, fn, useCapture){
  if (obj.addEventListener){
    obj.addEventListener(evType, fn, useCapture);
    return true;
  } else if (obj.attachEvent){
    var r = obj.attachEvent("on"+evType, fn);
    return r;
  } else {
    alert("Handler could not be attached");
  }
}

SellEventClass = function(){
	   var T = this;
	   this.prop1 = 100;
	   this.init = function(a){
	        addEvent(document.getElementById(a), "click", function(e){T.ActionOnClick.call(this,e)});
	   }
	   this.ActionOnClick = function(e){
		   //получаем название группы из селектора
		   //запускаем Форму с параметрами
		   
	   		alert(this.id);
	   };
	   return this;
}

function setTableORIOKS(nameGroup){
	
	var dayNames=["Monday","Tuesday","Wednesday","Thursday","Friday","Saturday","Sunday"];
	//Можно вынести в отдельный метод
	var schedule=$.parseJSON(getJSONFile("Schedule.json"));	
	var pairsForThisGroup=new Array();
	for (var i=0;i<schedule.length;i++){		
		var pos;
		pos= (schedule[i].Groups.indexOf(nameGroup)>-1);
		if (pos){
			pairsForThisGroup.push(schedule[i]);
		}
	}
	//return pairsForThisGroup;
	
	for (var i=0;i<pairsForThisGroup.length;i++){
		var pair=pairsForThisGroup[i];
		var pairNumberDay=dayNames.indexOf(pair.DayOfWeek);
		//I-ый числитель
		if (pair.FirstNumerator) {
			$("#td_"+pairNumberDay+"_"+(pair.Number-1)+"_0").text(pair.Discipline.Title+" к."+pair.Room);
		}
		//I-ой знаменатель
		if (pair.FirstDenumerator) {
			$("#td_"+pairNumberDay+"_"+(pair.Number-1)+"_1").text(pair.Discipline.Title+" к."+pair.Room);
		}
		//II-ый числитель
		if (pair.SecondNumerator) {
			$("#td_"+pairNumberDay+"_"+(pair.Number-1)+"_2").text(pair.Discipline.Title+" к."+pair.Room);
		}
		//II-ой знаменатель
		if (pair.SecondDenumerator) {
			$("#td_"+pairNumberDay+"_"+(pair.Number-1)+"_3").text(pair.Discipline.Title+" к."+pair.Room);
		}
		//console.log(pair);
	}
}
	
	
function loadJSONMySchedule(nameFileJSON){
	
};